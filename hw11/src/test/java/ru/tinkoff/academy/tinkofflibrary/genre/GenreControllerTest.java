package ru.tinkoff.academy.tinkofflibrary.genre;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.shaded.com.fasterxml.jackson.core.type.TypeReference;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.tinkoff.academy.tinkofflibrary.AbstractIntegrationTest;
import ru.tinkoff.academy.tinkofflibrary.genre.dto.CreatingGenreDto;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class GenreControllerTest extends AbstractIntegrationTest {
    @Autowired
    private MockMvc mvc;

    @Test
    public void findAll() throws Exception {
        MvcResult result = mvc.perform(get("/genre")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        List<Genre> actual = new ObjectMapper().readValue(
                result.getResponse().getContentAsString(),
                new TypeReference<List<Genre>>() {
                }
        );
        assertTrue(actual.containsAll(provideGenres()));
    }

    private static List<Genre> provideGenres() {
        return List.of(
                Genre.builder().id(1L).name("Fantasy").build(),
                Genre.builder().id(3L).name("Historical fiction").build(),
                Genre.builder().id(4L).name("Horror").build(),
                Genre.builder().id(6L).name("Alternate history").build()
        );
    }

    @ParameterizedTest
    @MethodSource("provideGenres")
    public void getById(Genre expected) throws Exception {
        MvcResult result = mvc.perform(get("/genre/" + expected.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        Genre actual = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Genre.class);
        assertEquals(expected.getName(), actual.getName());
    }

    private static Stream<CreatingGenreDto> provideGenreDto() {
        return Stream.of(
                CreatingGenreDto.builder().name("Children's").build(),
                CreatingGenreDto.builder().name("Classic").build(),
                CreatingGenreDto.builder().name("Comic book").build(),
                CreatingGenreDto.builder().name("Coming-of-age").build()
        );
    }

    @ParameterizedTest
    @MethodSource("provideGenreDto")
    public void save(CreatingGenreDto genreDto) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        MvcResult result = mvc.perform(post("/genre")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(genreDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        Genre actual = mapper.readValue(result.getResponse().getContentAsString(), Genre.class);
        assertEquals(genreDto.getName(), actual.getName());
    }
}