package ru.tinkoff.academy.tinkofflibrary.author;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.shaded.com.fasterxml.jackson.core.type.TypeReference;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.tinkoff.academy.tinkofflibrary.AbstractIntegrationTest;
import ru.tinkoff.academy.tinkofflibrary.author.dto.CreatingAuthorDto;
import ru.tinkoff.academy.tinkofflibrary.genre.Genre;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AuthorControllerTest extends AbstractIntegrationTest {

    @Autowired
    private MockMvc mvc;

    private static List<Author> provideAuthors() {
        return List.of(
                Author.builder().id(1L).fullName("Stephen King").build(),
                Author.builder().id(2L).fullName("Ernest Hemingway").build(),
                Author.builder().id(3L).fullName("Charles Dickens").build(),
                Author.builder().id(4L).fullName("George Orwell").build()
        );
    }

    @Order(1)
    @Test
    public void findAll() throws Exception {
        MvcResult result = mvc.perform(get("/author")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        List<Author> actual = new ObjectMapper().readValue(
                result.getResponse().getContentAsString(),
                new TypeReference<List<Author>>() {
                }
        );
        assertEquals(provideAuthors(), actual);
    }

    @Order(2)
    @ParameterizedTest
    @MethodSource("provideAuthors")
    public void getById(Author expected) throws Exception {
        MvcResult result = mvc.perform(get("/author/" + expected.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        Author actual = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Author.class);
        assertEquals(expected.getFullName(), actual.getFullName());
    }

    private static Stream<CreatingAuthorDto> provideAuthorDto() {
        return Stream.of(
                CreatingAuthorDto.builder().fullName("Mark Twain").build(),
                CreatingAuthorDto.builder().fullName("J. K. Rowling").build(),
                CreatingAuthorDto.builder().fullName("F. Scott Fitzgerald").build()
        );
    }

    @Order(3)
    @ParameterizedTest
    @MethodSource("provideAuthorDto")
    public void save(CreatingAuthorDto genreDto) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        MvcResult result = mvc.perform(post("/author")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(genreDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        Author actual = mapper.readValue(result.getResponse().getContentAsString(), Author.class);
        assertEquals(genreDto.getFullName(), actual.getFullName());
    }
}