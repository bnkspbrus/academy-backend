package ru.tinkoff.academy.tinkofflibrary.book;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.testcontainers.shaded.com.fasterxml.jackson.core.JsonProcessingException;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.tinkoff.academy.tinkofflibrary.AbstractIntegrationTest;
import ru.tinkoff.academy.tinkofflibrary.author.Author;
import ru.tinkoff.academy.tinkofflibrary.book.dto.CreatingBookDto;
import ru.tinkoff.academy.tinkofflibrary.genre.Genre;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BookControllerTest extends AbstractIntegrationTest {

    @Autowired
    private MockMvc mvc;
    private static List<Book> provideBooks() {
        return List.of(
                Book.builder()
                        .genre(Genre.builder().id(1L).name("Fantasy").build())
                        .id(1L)
                        .title("The Great Gatsby")
                        .isVisible(true)
                        .authors(
                                List.of(Author.builder().id(1L).fullName("Stephen King").build())
                        )
                        .price(new BigDecimal("25.65"))
                        .build(),
                Book.builder()
                        .id(2L)
                        .title("To Kill a Mockingbird")
                        .genre(Genre.builder().id(2L).name("Graphic novel").build())
                        .isVisible(false)
                        .authors(
                                List.of(Author.builder().id(2L).fullName("Ernest Hemingway").build()
                                ))
                        .price(new BigDecimal("30.65"))
                        .build()
        );
    }

    @ParameterizedTest
    @MethodSource("provideBooks")
    void getById(Book expected) throws Exception {
        MvcResult result = mvc.perform(get("/books/" + expected.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        Book actual = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Book.class);
        assertEquals(expected.getTitle(), actual.getTitle());
    }

    private static Stream<CreatingBookDto> provideBookDto() {
        return Stream.of(
                CreatingBookDto
                        .builder()
                        .title("One Hundred Years of Solitude")
                        .authorIds(List.of(1L, 2L))
                        .genreId(1L)
                        .isVisible(true)
                        .price(new BigDecimal("33.3"))
                        .build()
        );
    }

    @ParameterizedTest
    @MethodSource("provideBookDto")
    void save(CreatingBookDto bookDto) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        MvcResult result = mvc.perform(post("/books")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(bookDto)))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        Book actual = mapper.readValue(result.getResponse().getContentAsString(), Book.class);
        assertEquals(bookDto.getTitle(), actual.getTitle());
    }
}