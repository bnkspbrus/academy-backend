insert into genre(name)
values ('Fantasy');
insert into genre(name)
values ('Graphic novel');
insert into genre(name)
values ('Historical fiction');
insert into genre(name)
values ('Horror');
insert into genre(name)
values ('Action and adventure');
insert into genre(name)
values ('Alternate history');
insert into genre(name)
values ('Anthology');
insert into genre(name)
values ('Chick lit');


insert into author(full_name)
values ('Stephen King');
insert into author(full_name)
values ('Ernest Hemingway');
insert into author(full_name)
values ('Charles Dickens');
insert into author(full_name)
values ('George Orwell');

insert into book(title, price, is_visible, genre_id)
values ('The Great Gatsby', 25.65, true, 1);
insert into book(title, price, is_visible, genre_id)
values ('To Kill a Mockingbird', 30.65, true, 2);
insert into book(title, price, is_visible, genre_id)
values ('The Catcher in the Rye', 10.10, false, 3);

insert into book_author(book_id, author_id)
values (1, 1);
insert into book_author(book_id, author_id)
values (2, 2);
insert into book_author(book_id, author_id)
values (3, 1);