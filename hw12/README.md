Я использую springdoc-openapi для swagger.

OpenAPI файл конфигурации генирится автоматически когда запускается приложение. Он генерится по
адресу <http://localhost:8080/v3/api-docs.yaml>.

UML диграмма: [tinkofflibrary.png](tinkofflibrary.png)