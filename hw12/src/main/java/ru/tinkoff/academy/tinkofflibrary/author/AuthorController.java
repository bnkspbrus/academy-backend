package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.academy.tinkofflibrary.author.dto.CreatingAuthorDto;

import java.util.List;

/**
 * Author Rest controller
 */
@RestController
@RequestMapping("/author")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    /**
     * Returns list of all authors
     *
     * @return list of all authors
     */
    @GetMapping
    public List<Author> findAll() {
        return authorService.findAll();
    }

    /**
     * Returns single author with given id which is path variable
     *
     * @param authorId id of author to be returned
     * @return author with authorId
     */
    @GetMapping("/{authorId}")
    public Author getById(@PathVariable Long authorId) {
        return authorService.getById(authorId);
    }

    /**
     * Saves single author in bookstore
     *
     * @param authorDto dto to create author for saving
     * @return saved author
     */
    @PostMapping
    public Author save(@RequestBody CreatingAuthorDto authorDto) {
        return authorService.save(authorDto);
    }

}
