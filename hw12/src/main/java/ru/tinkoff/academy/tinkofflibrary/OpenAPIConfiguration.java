package ru.tinkoff.academy.tinkofflibrary;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        info = @Info(
                title = "Bookstore",
                description = "A sample bookstore"),
        servers = @Server(url = "http://localhost:8080")
)
public class OpenAPIConfiguration {
}
