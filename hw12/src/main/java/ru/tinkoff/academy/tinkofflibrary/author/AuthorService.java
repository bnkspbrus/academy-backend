package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.author.dto.CreatingAuthorDto;
import ru.tinkoff.academy.tinkofflibrary.core.exception.EntityNotFoundException;

import java.util.List;

/**
 * Author service
 */
@Service
@RequiredArgsConstructor
public class AuthorService {

    private final ModelMapper modelMapper;

    private final AuthorRepository authorRepository;

    /**
     * Returns author with given id
     *
     * @param authorId author id to return
     * @return author with authorId
     */
    public Author getById(Long authorId) {
        return authorRepository.findById(authorId)
                .orElseThrow(() -> new EntityNotFoundException("Author didn't found by id=" + authorId));
    }

    /**
     * Returns list of authors with their ids
     *
     * @param authorIds ids of authors to return
     * @return list of authors with authorIds
     */
    public List<Author> findAllByIds(List<Long> authorIds) {
        return authorRepository.findAllById(authorIds);
    }

    /**
     * Returns all authors in storage
     *
     * @return list of all authors
     */
    public List<Author> findAll() {
        return authorRepository.findAll();
    }

    /**
     * Saves author is storage
     *
     * @param authorDto dto to create author for saving
     * @return saved author
     */
    public Author save(CreatingAuthorDto authorDto) {
        // MapStruct
        Author author = modelMapper.map(authorDto, Author.class);

        return authorRepository.save(author);
    }
}
