package ru.tinkoff.academy.tinkofflibrary.author;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Author repository
 */
@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

}
