package ru.tinkoff.academy.tinkofflibrary.genre;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.core.exception.EntityNotFoundException;
import ru.tinkoff.academy.tinkofflibrary.genre.dto.CreatingGenreDto;

import java.util.List;

/**
 * Genre service
 */
@Service
@RequiredArgsConstructor
public class GenreService {

    private final ModelMapper modelMapper;
    private final GenreRepository genreRepository;

    /**
     * Returns genre with given id
     *
     * @param genreId genre id to return
     * @return genre with genreId
     */
    public Genre getById(Long genreId) {
        return genreRepository.findById(genreId)
                .orElseThrow(() -> new EntityNotFoundException("Genre didn't find by id=" + genreId));
    }

    /**
     * Returns all genres in storage
     *
     * @return list aff all genres
     */
    public List<Genre> findAll() {
        return genreRepository.findAll();
    }

    /**
     * Saves genre with given id in storage
     *
     * @param genreDto dto to create genre for saving
     * @return saved genre
     */
    public Genre save(CreatingGenreDto genreDto) {
        Genre author = modelMapper.map(genreDto, Genre.class);

        return genreRepository.save(author);
    }
}
