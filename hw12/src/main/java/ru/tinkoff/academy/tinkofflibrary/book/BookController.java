package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.academy.tinkofflibrary.book.dto.CreatingBookDto;
import ru.tinkoff.academy.tinkofflibrary.book.request.GettingRequest;

import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Book Rest controller.
 */
@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;

    /**
     * Searches books with given query params which is {@link Book} properties
     *
     * @param requestParameters book params in {@code Map}
     * @return book page
     */
    @GetMapping("/search")
    public Page<Book> search(@RequestParam Map<String, String> requestParameters) {
        int page = Integer.parseInt(requestParameters.getOrDefault("page", "0"));
        requestParameters.remove("page");

        return bookService.search(page, requestParameters);
    }

    /**
     * Returns page of books with given query params which is {@link GettingRequest} properties
     *
     * @param gettingRequest page params in {@code GettingRequest}
     * @return book page
     */
    @GetMapping
    public Page<Book> getByPage(GettingRequest gettingRequest) {
        return bookService.findByPage(gettingRequest);
    }

    /**
     * Returns single book with given id which is path variable
     *
     * @param bookId book id to return
     * @return book with given id
     */
    @GetMapping("/{bookId}")
    public Book getById(@PathVariable Long bookId) {
        return bookService.getById(bookId);
    }

    /**
     * Saves single book in bookstore
     *
     * @param creatingBookDto dto to create book for saving
     * @return saved book
     */
    @PostMapping
    public Book save(@RequestBody CreatingBookDto creatingBookDto) {
        return bookService.save(creatingBookDto);
    }

    /**
     * Deletes book with given id which is path variable
     *
     * @param bookId book id to delete
     */
    @DeleteMapping("/{bookId}")
    public void delete(@PathVariable Long bookId) {
        bookService.remove(bookId);
    }

}
