package ru.tinkoff.academy.tinkofflibrary.genre;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.academy.tinkofflibrary.genre.dto.CreatingGenreDto;

import java.util.List;

/**
 * Genre Rest controller
 */
@RestController
@RequestMapping("/genre")
@RequiredArgsConstructor
public class GenreController {

    private final GenreService genreService;

    /**
     * Returns list of all genres
     *
     * @return list of all genres
     */
    @GetMapping
    public List<Genre> findAll() {
        return genreService.findAll();
    }

    /**
     * Returns single genre with given id which is path variable
     *
     * @param genreId id of genre to be returned
     * @return genre with genreId
     */
    @GetMapping("/{genreId}")
    public Genre getById(@PathVariable Long genreId) {
        return genreService.getById(genreId);
    }

    /**
     * Saves single genre
     *
     * @param genreDto dto to create genre for saving
     * @return saved genre
     */
    @PostMapping
    public Genre save(@RequestBody CreatingGenreDto genreDto) {
        return genreService.save(genreDto);
    }

}
