package seminar.homework.optional;


import org.springframework.lang.Nullable;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Task1 {


    public List<Email> create() {
        Email noAttachment = new Email("First!", "No attachment", /*Optional.empty()*/ null);
        Attachment attachment = new Attachment("/mnt/files/image.png", 370);
        Email withAttachment = new Email("Second!", "With attachment", /*Optional.of(*/attachment/*)*/);
        return Arrays.asList(noAttachment, withAttachment);
    }

    class Email implements Serializable {
        private final String subject;
        private final String body;
        private final /*Optional<Attachment>* -- нехорошо делать поля Optional*/ Attachment attachment;

        Email(String subject, String body, /*Optional<Attachment>*/Attachment attachment) {
            this.subject = subject;
            this.body = body;
            this.attachment = attachment;
        }

        String getSubject() {
            return subject;
        }

        String getBody() {
            return body;
        }

        @Nullable
            // -- пометим метод как возвращающий null
        Attachment getAttachment() {
            return attachment;
        }
    }


    class Attachment {
        private final String path;
        private final int size;

        Attachment(String path, int size) {
            this.path = path;
            this.size = size;
        }

        String getPath() {
            return path;
        }

        int getSize() {
            return size;
        }
    }
}