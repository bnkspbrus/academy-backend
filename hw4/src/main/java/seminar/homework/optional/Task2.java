package seminar.homework.optional;

import seminar.User;

import java.util.List;
import java.util.Optional;

public class Task2 {
    private static final String ADMIN_ROLE = "admin";


    // Лучше придерживаться одного контракта и вернуть Optional
    public Optional<User> findAnyAdmin() {
//        Optional<List<User>> users = findUsersByRole(ADMIN_ROLE);
//        if (users.isPresent() && !users.get().isEmpty()) {
//            return users.get().get(0);
//        }
//        throw new IllegalStateException("No admins found");
        // Есть готовый метод.
        return findUsersByRole(ADMIN_ROLE).flatMap(list -> list.stream().findAny());
    }

    private Optional<List<User>> findUsersByRole(String role) {
        //real search in DB
        return Optional.empty();
    }


}