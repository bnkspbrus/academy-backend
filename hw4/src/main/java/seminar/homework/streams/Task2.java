package seminar.homework.streams;

import seminar.User;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Task2 {
    private final Set<User> users = new HashSet<>();

    public int getTotalAge() {
        return users.stream()
//                .map(User::getAge)
//                .reduce(0, Integer::sum);
                // Лучше использовать стрим примитивов с встроенной функцией sum().
                .mapToInt(User::getAge).sum();
    }

    public int countEmployees(Map<String, List<User>> departments) {
        return /*(int)*/ departments.values().stream()
//                .flatMap(List::stream)
//                .count();
                // Аналогично предыдущему. Можно использовать стрим примитивов. Не нужно делать flatMap только чтобы
                // посчитать длину потока. Для этого есть size().
                .mapToInt(List::size).sum();
    }
}