package seminar.homework.streams;

import seminar.User;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Task1 {

    public Map<String, Integer> getMaxAgeByUserName(List<User> users) {
        return users.stream()
//                .collect(groupingBy(User::getName))
//                .entrySet().stream()
//                .collect(toMap(
//                        Map.Entry::getKey,
//                        e -> e.getValue().stream()
//                                .map(User::getAge)
//                                .reduce(0, Integer::max)
//                ));
                // Здесь создается промежуточный Map, когда можно обойтись без него.
                // Можно просто использовать одну операцию collect, где извлекать имя и возраст.
                .collect(Collectors.toMap(User::getName, User::getAge, Integer::max));
    }
}