package seminar.homework.streams;


import seminar.User;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.IntStream;

public class Task3 {
    public void printNameStats(List<User> users) {
        IntSummaryStatistics intSummaryStatistics = getNameLengthStream(users)
//                .max()
//                .ifPresent(max -> System.out.println("MAX: " + max));
//        getNameLengthStream(users)
//                .min()
//                .ifPresent(min -> System.out.println("MIN: " + min));
                .summaryStatistics();
        System.out.println("MAX: " + intSummaryStatistics.getMax());
        System.out.println("MIN: " + intSummaryStatistics.getMin());
        // Можно одной терминальной операцией получить все статистики сразу.
    }

    private IntStream getNameLengthStream(List<User> users) {
        return users.stream()
                .mapToInt(user -> user.getName().length());
    }
}