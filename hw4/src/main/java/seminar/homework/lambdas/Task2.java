package seminar.homework.lambdas;

import seminar.Role;
import seminar.User;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;

public class Task2 {
    private final Map<String, Set<User>> usersByRole = new HashMap<>();

    public void addUser(User user) {
//        user.getRoles().forEach(r -> {
//            Set<User> usersInRole = usersByRole.get(r.getName());
//            if (usersInRole == null) {
//                usersInRole = new HashSet<>();
//                usersByRole.put(r.getName(), usersInRole);
//            }
//            usersInRole.add(user);
//        });
        // Есть готовый метод computeIfAbsent
        user.getRoles().forEach(role -> {
            Set<User> usersInRole = usersByRole.computeIfAbsent(role.getName(), k -> {
                Set<User> result = new HashSet<>();
                usersByRole.put(role.getName(), result);
                return result;
            });
            usersInRole.add(user);
        });
    }

    public Set<User> getUsersInRole(String role) {
        // Есть готовый метод.
        return usersByRole.getOrDefault(role, Collections.emptySet());
        //Set<User> users = usersByRole.get(role);
        //return users == null ? Collections.emptySet() : users;
    }
}