package seminar.homework.lambdas;

import seminar.Permission;
import seminar.User;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Task1 {
    private final Set<User> users = new HashSet<>();

    public void removeUsersWithPermission(Permission permission) {
        //        while (iterator.hasNext()) {
//            User user = iterator.next();
//            if (user.getRoles().stream()
//                    .anyMatch(r -> r.getPermissions().contains(permission))) {
//                iterator.remove();
//            }
//        }
        users.removeIf(user -> user.getRoles().stream()
                .anyMatch(r -> r.getPermissions().contains(permission)));
        // Лучше использовать готовую функцию, принимающую предикат.
    }
}