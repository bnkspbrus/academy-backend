package board;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static board.Cell.O;
import static board.Cell.X;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class TicTacToeBoardTest {

    private static final Cell[][] testCells = new Cell[][]{{X, null, null}, {null, O, O}, {X, null, null}};
    private Board testBoard;

    @BeforeEach
    public void setUp() {
        testBoard = new TicTacToeBoard(testCells);
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForTestIsValidMove")
    public void testIsValidMove(BoardPoint point, boolean expectValid) {
        boolean actual = testBoard.isValidMove(point);
        assertEquals(expectValid, actual);
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForTestMoveResult")
    public void testMoveResult(Cell[][] cells, BoardPoint point, MoveResult expectedMoveResultWithPoint) {
        TicTacToeBoard board = new TicTacToeBoard(cells);
        MoveResult actual = board.doMove(point);
        assertEquals(expectedMoveResultWithPoint, actual);
    }

    @Test
    public void testDoMove() {
        testBoard.doMove(new BoardPoint(0, 1));
        testBoard.doMove(new BoardPoint(0, 2));
        TicTacToeBoard expected = new TicTacToeBoard(new Cell[][]{{X, X, O}, {null, O, O}, {X, null, null}});
        assertEquals(expected, testBoard);
    }

    @Test
    public void testMakeMutableCopy() {
        Board boardCopy = testBoard.makeMutableCopy();
        boardCopy.doMove(new BoardPoint(0, 1));
        boardCopy.doMove(new BoardPoint(0, 2));
        assertNotEquals(testBoard, boardCopy);
    }

    private static Stream<Arguments> provideArgumentsForTestIsValidMove() {
        return Stream.of(
                Arguments.of(new BoardPoint(0, 0), false),
                Arguments.of(new BoardPoint(0, 1), true),
                Arguments.of(new BoardPoint(0, 2), true),
                Arguments.of(new BoardPoint(1, 0), true),
                Arguments.of(new BoardPoint(1, 1), false),
                Arguments.of(new BoardPoint(1, 2), false),
                Arguments.of(new BoardPoint(2, 0), false),
                Arguments.of(new BoardPoint(2, 1), true),
                Arguments.of(new BoardPoint(2, 2), true),
                Arguments.of(new BoardPoint(10, 10), false)
        );
    }

    private static Stream<Arguments> provideArgumentsForTestMoveResult() {
        return Stream.of(
                Arguments.of(
                        new Cell[][]{{O, null, null}, {X, X, null}, {O, null, null}},
                        new BoardPoint(1, 2),
                        MoveResult.WIN
                ),
                Arguments.of(
                        new Cell[][]{{O, X, null}, {null, X, null}, {null, null, O}},
                        new BoardPoint(2, 1),
                        MoveResult.WIN
                ),
                Arguments.of(
                        new Cell[][]{{X, X, null}, {O, O, null}, {null, null, null}},
                        new BoardPoint(0, 2),
                        MoveResult.WIN
                ),
                Arguments.of(
                        new Cell[][]{{X, O, null}, {X, O, null}, {null, null, null}},
                        new BoardPoint(2, 0),
                        MoveResult.WIN
                ),
                Arguments.of(
                        new Cell[][]{{O, null, X}, {null, null, X}, {O, null, null}},
                        new BoardPoint(2, 2),
                        MoveResult.WIN
                ),
                Arguments.of(
                        new Cell[][]{{O, null, X}, {null, O, null}, {null, null, X}},
                        new BoardPoint(1, 2),
                        MoveResult.WIN
                ),
                Arguments.of(
                        new Cell[][]{{O, null, X}, {null, null, null}, {X, null, O}},
                        new BoardPoint(1, 1),
                        MoveResult.WIN
                ),
                Arguments.of(
                        new Cell[][]{{X, O, null}, {O, X, null}, {null, null, null}},
                        new BoardPoint(2, 2),
                        MoveResult.WIN
                ),
                Arguments.of(
                        new Cell[][]{{X, O, null}, {O, X, null}, {null, null, null}},
                        new BoardPoint(2, 0),
                        MoveResult.UNKNOWN
                ),
                Arguments.of(
                        new Cell[][]{{X, O, null}, {O, X, null}, {null, null, null}},
                        new BoardPoint(2, 1),
                        MoveResult.UNKNOWN
                ),
                Arguments.of(
                        new Cell[][]{{X, O, null}, {O, X, null}, {null, null, null}},
                        new BoardPoint(0, 2),
                        MoveResult.UNKNOWN
                )
        );
    }
}