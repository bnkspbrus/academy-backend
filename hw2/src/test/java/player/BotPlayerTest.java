package player;

import board.Board;
import board.BoardPoint;
import board.Cell;
import board.MoveResult;
import board.TicTacToeBoard;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static board.Cell.O;
import static board.Cell.X;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BotPlayerTest {

    @ParameterizedTest
    @MethodSource("provideArgumentsForTestObviousWinMove")
    public void testObviousWinMove(Cell[][] cells) {
        Board board = new TicTacToeBoard(cells);
        BotPlayer player = new BotPlayer();
        BoardPoint move = player.doMove(board);
        MoveResult actual = board.doMove(move);
        MoveResult expected = MoveResult.WIN;
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForTestObviousBlockMove")
    public void testObviousBlockMove(Cell[][] cells, BoardPoint expected) {
        Board board = new TicTacToeBoard(cells);
        BotPlayer player = new BotPlayer();
        BoardPoint actual = player.doMove(board);
        assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForTestObviousLoss")
    public void testObviousLoss(Cell[][] cells) {
        Board board = new TicTacToeBoard(cells);
        BotPlayer looser = new BotPlayer();
        BotPlayer winner = new BotPlayer();
        BoardPoint looserMove = looser.doMove(board);
        MoveResult actualLooserMoveResult = board.doMove(looserMove);
        MoveResult expectedLooserMoveResult = MoveResult.UNKNOWN;
        assertEquals(expectedLooserMoveResult, actualLooserMoveResult);
        BoardPoint winnerMove = winner.doMove(board);
        MoveResult actualWinnerMoveResult = board.doMove(winnerMove);
        MoveResult expectedWinnerMoveResult = MoveResult.WIN;
        assertEquals(expectedWinnerMoveResult, actualWinnerMoveResult);
    }

    private static Stream<Cell[][]> provideArgumentsForTestObviousWinMove() {
        return Stream.of(
                new Cell[][]{{O, null, null}, {X, X, null}, {O, null, null}},
                new Cell[][]{{O, X, null}, {null, X, null}, {null, null, O}},
                new Cell[][]{{X, X, null}, {O, O, null}, {null, null, null}},
                new Cell[][]{{X, O, null}, {X, O, null}, {null, null, null}},
                new Cell[][]{{O, null, X}, {null, null, X}, {O, null, null}},
                new Cell[][]{{O, null, X}, {null, O, null}, {null, null, X}},
                new Cell[][]{{O, null, X}, {null, null, null}, {X, null, O}},
                new Cell[][]{{X, O, null}, {O, X, null}, {null, null, null}}
        );
    }

    private static Stream<Arguments> provideArgumentsForTestObviousBlockMove() {
        return Stream.of(
                Arguments.of(
                        new Cell[][]{{X, null, X}, {O, null, null}, {null, null, null}},
                        new BoardPoint(0, 1)
                ),
                Arguments.of(
                        new Cell[][]{{null, O, null}, {X, X, null}, {null, null, null}},
                        new BoardPoint(1, 2)
                ),
                Arguments.of(
                        new Cell[][]{{null, null, O}, {null, null, null}, {null, X, X}},
                        new BoardPoint(2, 0)
                ),
                Arguments.of(
                        new Cell[][]{{X, null, null}, {null, O, null}, {X, null, null}},
                        new BoardPoint(1, 0)
                ),
                Arguments.of(
                        new Cell[][]{{O, null, null}, {null, X, null}, {null, X, null}},
                        new BoardPoint(0, 1)
                ),
                Arguments.of(
                        new Cell[][]{{O, null, X}, {null, null, X}, {null, null, null}},
                        new BoardPoint(2, 2)
                ),
                Arguments.of(
                        new Cell[][]{{X, null, O}, {null, X, null}, {null, null, null}},
                        new BoardPoint(2, 2)
                ),
                Arguments.of(
                        new Cell[][]{{null, O, null}, {null, X, null}, {X, null, null}},
                        new BoardPoint(0, 2)
                )
        );
    }

    private static Stream<Cell[][]> provideArgumentsForTestObviousLoss() {
        return Stream.of(
                new Cell[][]{{X, X, O}, {O, X, null}, {null, null, null}},
                new Cell[][]{{X, O, null}, {X, X, null}, {O, null, null}},
                new Cell[][]{{O, X, O}, {null, null, null}, {null, X, X}},
                new Cell[][]{{X, null, null}, {O, X, O}, {X, null, null}},
                new Cell[][]{{X, O, O}, {null, X, null}, {X, null, null}},
                new Cell[][]{{O, X, O}, {null, X, X}, {null, null, null}},
                new Cell[][]{{X, O, null}, {null, null, O}, {X, null, X}},
                new Cell[][]{{O, null, null}, {null, X, X}, {X, O, null}}
        );
    }
}