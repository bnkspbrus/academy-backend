package player;

import board.Board;
import board.BoardPoint;
import board.Cell;
import board.TicTacToeBoard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static board.Cell.O;
import static board.Cell.X;
import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static com.github.stefanbirkner.systemlambda.SystemLambda.withTextFromSystemIn;
import static org.junit.jupiter.api.Assertions.*;

class HumanPlayerTest {

    private static final int TEST_BOARD_SIZE = 3;
    private Board testBoard;

    @BeforeEach
    public void setUp() {
        testBoard = new TicTacToeBoard(
                new Cell[][]{{X, null, null}, {null, O, O}, {X, null, null}}
        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForTestUserInput")
    public void testUserInput(BoardPoint point) throws Exception {
        withTextFromSystemIn((point.ordinate() + 1) + " " + (point.abscissa() + 1)).execute(() -> {
            String output = tapSystemOut(() -> {
                try {
                    testDoMove(point);
                } catch (NoSuchElementException ignore) {
                    assertFalse(testBoard.isValidMove(point));
                }
            });
            if (testBoard.isValidMove(point)) {
                assertFalse(output.contains(HumanPlayer.INVALID_COORDINATES_MESSAGE));
            } else {
                assertTrue(output.contains(HumanPlayer.INVALID_COORDINATES_MESSAGE));
            }
        });
    }

    private static Stream<BoardPoint> provideArgumentsForTestUserInput() {
        return IntStream
                .range(0, TEST_BOARD_SIZE)
                .mapToObj(i -> IntStream
                        .range(0, TEST_BOARD_SIZE)
                        .mapToObj(j -> new BoardPoint(i, j)))
                .flatMap(Function.identity());
    }

    private void testDoMove(BoardPoint expected) {
        BoardPoint actual = new HumanPlayer(new Scanner(System.in)).doMove(testBoard);
        assertEquals(expected, actual);
    }
}