import board.Board;
import board.BoardPoint;
import board.MoveResult;
import board.TicTacToeBoard;
import player.BotPlayer;
import player.HumanPlayer;
import player.Player;
import utils.GameUtils;
import utils.ScannerUtils;

import java.util.Scanner;

public class TicTacToe {
    private final Player player1;
    private final Player player2;
    private final Board board;

    public TicTacToe(Board board, Player player1, Player player2) {
        this.board = board;
        this.player1 = player1;
        this.player2 = player2;
    }

    public static TicTacToe newGameWithBot() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter size of board: ");
        int size;
        do {
            size = ScannerUtils.nextIntAndAdvanceNextLine(scanner);
            if (size < GameUtils.WINNING_STRIPE_LENGTH) {
                System.out.println("Size must be at least " + GameUtils.WINNING_STRIPE_LENGTH + ". Try again.");
            } else {
                break;
            }
        } while (true);
        TicTacToeBoard board = new TicTacToeBoard(size);
        return new TicTacToe(board, new HumanPlayer(scanner), new BotPlayer());
    }

    public void doGame() {
        while (true) {
            BoardPoint point = player1.doMove(board);
            MoveResult moveResult = board.doMove(point);
            if (moveResult != MoveResult.UNKNOWN) {
                if (moveResult == MoveResult.WIN) {
                    System.out.println("X won!");
                } else {
                    System.out.println("Draw!");
                }
                System.out.println(board);
                return;
            }
            point = player2.doMove(board);
            moveResult = board.doMove(point);
            if (moveResult != MoveResult.UNKNOWN) {
                if (moveResult == MoveResult.WIN) {
                    System.out.println("O won!");
                } else {
                    System.out.println("Draw!");
                }
                System.out.println(board);
                return;
            }
        }
    }

    public static void main(String[] args) {
        TicTacToe ticTacToe = TicTacToe.newGameWithBot();
        ticTacToe.doGame();
    }
}
