package utils;

import java.util.Scanner;

/**
 * Auxiliary util class for Scanner.
 */
public class ScannerUtils {
    /**
     * Execute Scanner's nextInt() and read all the rest line.
     *
     * @param scanner scanner to read next int.
     * @return result of action.
     */
    public static int nextIntAndAdvanceNextLine(Scanner scanner) {
        int result = scanner.nextInt();
        scanner.nextLine();
        return result;
    }
}
