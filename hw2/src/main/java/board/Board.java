package board;

/**
 * Mutable board. Can be modified with method {@link #doMove(BoardPoint)}  doMove}.
 */
public interface Board extends ImmutableBoard {
    /**
     * Method for player's move.
     *
     * @param point point on the board to move.
     * @return result of move.
     */
    MoveResult doMove(BoardPoint point);
}
