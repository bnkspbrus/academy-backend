package board;

/**
 * Record to be used as coordinate of cell in the board.
 *
 * @param ordinate vertical coordinate.
 * @param abscissa horizontal coordinate.
 */
public record BoardPoint(int ordinate, int abscissa) {
}
