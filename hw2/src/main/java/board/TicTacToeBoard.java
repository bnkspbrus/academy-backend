package board;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static utils.GameUtils.WINNING_STRIPE_LENGTH;

/**
 * Board for TicTacToe.
 */
public class TicTacToeBoard implements Board {
    private Cell turn;
    private final int size;
    private final Cell[][] cells;
    private long emptyCellsCount;

    public TicTacToeBoard(int size) {
        cells = new Cell[size][size];
        this.size = size;
        turn = Cell.X;
        emptyCellsCount = (long) size * size;
    }

    public TicTacToeBoard(Cell[][] cells) {
        this.cells = makeCellsCopy(cells);
        size = cells.length;
        CellsStats stats = getCellsStats(cells);
        if (stats.xCount == stats.oCount) {
            turn = Cell.X;
        } else if (stats.xCount == stats.oCount + 1) {
            turn = Cell.O;
        } else {
            throw new IllegalArgumentException("Invalid board");
        }
        emptyCellsCount = stats.emptyCount;
    }

    private TicTacToeBoard(int size, Cell[][] cells, Cell turn, long emptyCellsCount) {
        this.size = size;
        this.cells = makeCellsCopy(cells);
        this.turn = turn;
        this.emptyCellsCount = emptyCellsCount;
    }

    private Cell[][] makeCellsCopy(Cell[][] cells) {
        return Arrays.stream(cells).map(Cell[]::clone).toArray(Cell[][]::new);
    }

    private record CellsStats(long xCount, long oCount, long emptyCount) {
    }

    private CellsStats getCellsStats(Cell[][] cells) {
        Map<Cell, Long> grouped = Arrays.stream(cells)
                .flatMap(Arrays::stream)
                .filter(Objects::nonNull)
                .collect(groupingBy(identity(), () -> new EnumMap<>(Cell.class), counting()));
        long xCount = grouped.get(Cell.X) == null ? 0 : grouped.get(Cell.X);
        long oCount = grouped.get(Cell.O) == null ? 0 : grouped.get(Cell.O);
        long emptyCount = (long) cells.length * cells.length - xCount - oCount;
        return new CellsStats(xCount, oCount, emptyCount);
    }

    @Override
    public Cell getTurn() {
        return turn;
    }

    @Override
    public int getSize() {
        return size;
    }

    private Cell getCell(BoardPoint point) {
        return cells[point.ordinate()][point.abscissa()];
    }

    @Override
    public boolean isValidMove(BoardPoint point) {
        if (!isValidOrdinate(point.ordinate())) {
            return false;
        }
        if (!isValidAbscissa(point.abscissa())) {
            return false;
        }
        return getCell(point) == null;
    }

    private boolean isValidOrdinate(int ordinate) {
        return ordinate >= 0 && ordinate < size;
    }

    private boolean isValidAbscissa(int abscissa) {
        return abscissa >= 0 && abscissa < size;
    }

    private MoveResult getMoveResult(BoardPoint point) {
        for (int k = 2; k >= 0; k--) {
            if (point.ordinate() - k >= 0) {
                if (point.ordinate() - k + 2 < size) {
                    boolean accumulator = true;
                    for (int i = 0; i < WINNING_STRIPE_LENGTH; i++) {
                        accumulator = accumulator && (cells[point.ordinate() - k + i][point.abscissa()] == turn);
                    }
                    if (accumulator) {
                        return MoveResult.WIN;
                    }
                }
            }
        }
        for (int k = 2; k >= 0; k--) {
            if (point.abscissa() - k >= 0) {
                if (point.abscissa() - k + 2 < size) {
                    boolean accumulator = true;
                    for (int i = 0; i < WINNING_STRIPE_LENGTH; i++) {
                        accumulator = accumulator && (cells[point.ordinate()][point.abscissa() - k + i] == turn);
                    }
                    if (accumulator) {
                        return MoveResult.WIN;
                    }
                }
            }
        }
        for (int k = 2; k >= 0; k--) {
            if (point.ordinate() - k >= 0 && point.abscissa() - k >= 0) {
                if (point.ordinate() - k + 2 < size && point.abscissa() - k + 2 < size) {
                    boolean accumulator = true;
                    for (int i = 0; i < WINNING_STRIPE_LENGTH; i++) {
                        accumulator =
                                accumulator && (cells[point.ordinate() - k + i][point.abscissa() - k + i] == turn);
                    }
                    if (accumulator) {
                        return MoveResult.WIN;
                    }
                }
            }
        }
        for (int k = 2; k >= 0; k--) {
            if (point.ordinate() - k >= 0 && point.abscissa() + k < size) {
                if (point.ordinate() - k + 2 < size && point.abscissa() + k - 2 >= 0) {
                    boolean accumulator = true;
                    for (int i = 0; i < WINNING_STRIPE_LENGTH; i++) {
                        accumulator =
                                accumulator && (cells[point.ordinate() - k + i][point.abscissa() + k - i] == turn);
                    }
                    if (accumulator) {
                        return MoveResult.WIN;
                    }
                }
            }
        }
        if (emptyCellsCount == 0) {
            return MoveResult.DRAW;
        }
        return MoveResult.UNKNOWN;
    }

    @Override
    public Board makeMutableCopy() {
        return new TicTacToeBoard(size, cells, turn, emptyCellsCount);
    }

    @Override
    public MoveResult doMove(BoardPoint point) {
        if (!isValidMove(point)) {
            throw new IllegalArgumentException("Invalid move.");
        }
        cells[point.ordinate()][point.abscissa()] = turn;
        emptyCellsCount--;
        MoveResult moveResult = getMoveResult(point);
        turn = turn.invert();
        return moveResult;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("  ").append("_".repeat(size * 2 - 1));
        for (int i = 0; i < size; i++) {
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append(i + 1).append("|");
            for (Cell cell : cells[i]) {
                stringBuilder.append(cell == null ? "_" : cell).append("|");
            }
        }
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append("  ");
        for (int i = 0; i < size; i++) {
            stringBuilder.append(i + 1).append(" ");
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (other == null || getClass() != other.getClass())
            return false;
        TicTacToeBoard that = (TicTacToeBoard) other;
        return Arrays.deepEquals(cells, that.cells);
    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(cells);
    }
}
