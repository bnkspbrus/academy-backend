package board;

import java.util.EnumMap;
import java.util.Map;

/**
 * Result of player's move.
 */
public enum MoveResult {
    UNKNOWN, LOSS, DRAW, WIN;

    /**
     * Inverse move results for minimax algorithm.
     */
    private static final Map<MoveResult, MoveResult> NEGATE = new EnumMap<>(MoveResult.class);

    static {
        NEGATE.put(WIN, LOSS);
        NEGATE.put(DRAW, DRAW);
        NEGATE.put(LOSS, WIN);
    }

    public MoveResult negate() {
        return NEGATE.get(this);
    }
}
