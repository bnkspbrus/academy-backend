package board;

/**
 * Cell for TicTacToe board.
 */
public enum Cell {
    X, O;

    public Cell invert() {
        return this == Cell.X ? Cell.O : Cell.X;
    }
}
