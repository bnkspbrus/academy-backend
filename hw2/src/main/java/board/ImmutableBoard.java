package board;

public interface ImmutableBoard {

    Cell getTurn();

    int getSize();

    boolean isValidMove(BoardPoint point);

    Board makeMutableCopy();
}
