package minimax;

import board.BoardPoint;
import board.MoveResult;

/**
 * Returnable result of minimax algorithm. Just for convenience.
 *
 * @param moveResult result of game of this branch of minimax recursion.
 * @param depth      if all move results are not WIN we need to choose result with maximal depth to play as long
 *                   as possible, even if finally you will lose.
 * @param point      point to move.
 */
public record MinimaxResult(
        MoveResult moveResult,
        int depth,
        BoardPoint point
) implements Comparable<MinimaxResult> {
    @Override
    public int compareTo(MinimaxResult other) {
        int compare = moveResult.compareTo(other.moveResult());
        if (compare != 0) {
            return compare;
        }
        if (moveResult == MoveResult.WIN) {
            return Integer.compare(other.depth, depth);
        } else {
            return Integer.compare(depth, other.depth);
        }
    }
}