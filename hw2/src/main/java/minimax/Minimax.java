package minimax;

import board.Board;
import board.BoardPoint;
import board.ImmutableBoard;
import board.MoveResult;

import java.util.HashMap;
import java.util.Map;

public class Minimax {

    /**
     * Cached minimax results not to derive one result twice.
     */
    private final Map<ImmutableBoard, MinimaxResult> cachedMinimaxResults = new HashMap<>();

    /**
     * Minimax algorithm.
     *
     * @param board node of tree of minimax recursion.
     * @return result of minimax for this board.
     */
    public MinimaxResult doMinimax(ImmutableBoard board) {
        if (cachedMinimaxResults.containsKey(board)) {
            return cachedMinimaxResults.get(board);
        }
        MinimaxResult maximum = new MinimaxResult(MoveResult.LOSS, 0, null);
        // iterates for all possible combinations
        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++) {
                BoardPoint point = new BoardPoint(i, j);
                if (board.isValidMove(point)) {
                    MinimaxResult minimaxResult = doMoveAndThenDoMinimax(board, point);
                    if (maximum.compareTo(minimaxResult) <= 0) {
                        maximum = minimaxResult;
                    }
                }
            }
        }
        cachedMinimaxResults.put(board, maximum);
        return maximum;
    }

    private MinimaxResult doMoveAndThenDoMinimax(ImmutableBoard board, BoardPoint point) {
        Board boardCopy = board.makeMutableCopy();
        MoveResult moveResult = boardCopy.doMove(point);
        //  if it is leaf we just return minimax result.
        if (moveResult != MoveResult.UNKNOWN) {
            return new MinimaxResult(moveResult, 1, point);
            // else we do minimax
        } else {
            MinimaxResult negated = doNegatedMinimax(boardCopy);
            return new MinimaxResult(negated.moveResult(), negated.depth() + 1, point);

        }
    }

    private MinimaxResult doNegatedMinimax(ImmutableBoard board) {
        MinimaxResult minimaxResult = doMinimax(board);
        return new MinimaxResult(minimaxResult.moveResult().negate(), minimaxResult.depth(), minimaxResult.point());
    }
}
