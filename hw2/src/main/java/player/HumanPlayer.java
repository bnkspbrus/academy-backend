package player;

import board.BoardPoint;
import board.ImmutableBoard;

import java.util.Scanner;

import static utils.ScannerUtils.nextIntAndAdvanceNextLine;

public class HumanPlayer implements Player {
    public static final String INVALID_COORDINATES_MESSAGE = "Invalid coordinates. Try again.";
    public static final String ENTER_COORDINATES_MESSAGE = "Enter vertical and horizontal coordinates separated with " +
            "space: ";
    private final Scanner scanner;

    public HumanPlayer(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public BoardPoint doMove(ImmutableBoard board) {
        System.out.println("Your turn! You play for " + board.getTurn() + ".");
        System.out.println(board);
        do {
            System.out.print(ENTER_COORDINATES_MESSAGE);
            int ordinate = scanner.nextInt();
            int abscissa = nextIntAndAdvanceNextLine(scanner);
            BoardPoint point = new BoardPoint(--ordinate, --abscissa);
            if (board.isValidMove(point)) {
                return point;
            } else {
                System.out.println(INVALID_COORDINATES_MESSAGE);
            }
        } while (true);
    }
}
