package player;

import board.BoardPoint;
import board.ImmutableBoard;

/**
 *  Player who cans move.
 */
public interface Player {
    /**
     *
     * @param board immutable board just for understanding of possible moves.
     * @return board point to move.
     */
    BoardPoint doMove(ImmutableBoard board);
}
