package player;

import board.BoardPoint;
import board.ImmutableBoard;
import minimax.Minimax;

public class BotPlayer implements Player {
    private final Minimax minimax = new Minimax();

    @Override
    public BoardPoint doMove(ImmutableBoard board) {
        return minimax.doMinimax(board).point();
    }
}
