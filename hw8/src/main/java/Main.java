import reactor.core.publisher.Flux;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;


/**
 * В этой домашней работе вам необходимо найти в большом txt файле с книгами данные об авторах и названиях произведений.
 * Сделать это необходимо двумя способами: обычным последовательным и реактивным, с использованием библиотеки reactor
 * .core
 * При этом нужно собрать статистику какой из подходов более эффективен по расходу ресурсов.
 * Советую обратить внимание на доклад:
 * <p><a href="https://www.youtube.com/watch?v=tjp8pTOyiWg">Максим Гореликов — Дизайн реактивной системы на Spring 5/Reactor</a>
 * <p>Данные о задействованных ресурсах можно собрать к примеру с помощью {@link Runtime}.</p>
 * <p>Для RAM: {@link Runtime#totalMemory()} и {@link Runtime#freeMemory()}</p>
 */
public class Main {
    public static void main(String[] args) {
        Runtime runtime = Runtime.getRuntime();
        long startMemory = runtime.freeMemory();
        String fileName = System.getProperty("user.home") + "/books.txt";
        Path path = Paths.get(fileName);
        System.out.println("=".repeat(10) + "Reactive version" + "=".repeat(10) + "\n");
        long startTime = System.nanoTime();
        reactiveVersion(path);
        long reactiveTime = System.nanoTime() - startTime;
        long reactiveMemory = startMemory - runtime.freeMemory();
        System.out.println("\n" + "=".repeat(12) + "List version" + "=".repeat(12) + "\n");
        startMemory = runtime.freeMemory();
        startTime = System.nanoTime();
        listVersion(path);
        long listTime = System.nanoTime() - startTime;
        long listMemory = startMemory - runtime.freeMemory();
        System.out.println("\nReactive time: " + reactiveTime + ", List time: " + listTime);
        System.out.println("\nReactive memory: " + reactiveMemory + ", List memory: " + listMemory);
    }

    private static void reactiveVersion(Path path) {
        try (Stream<String> lines = Files.lines(path)) {
            Flux.fromStream(lines)
                    .filter(s -> s.contains("Author:") || s.contains("Title:"))
                    .map(String::trim)
                    .subscribe(System.out::println);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void listVersion(Path path) {
        try (Stream<String> lines = Files.lines(path)) {
            lines
                    .filter(s -> s.contains("Author:") || s.contains("Title:"))
                    .map(String::trim)
                    .forEach(System.out::println);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}