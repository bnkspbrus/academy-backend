package encoder;

public class StringEncoder implements Encoder {

    @Override
    public String encode(String string, int maxSize) {
        do {
            string = xorAdjacent(string);
        } while (string.length() > maxSize);
        return string;
    }

    private String xorAdjacent(String string) {
        char[] chars = string.toCharArray();
        char[] newChars;
        if (chars.length % 2 == 0) {
            newChars = new char[chars.length / 2];
        } else {
            newChars = new char[chars.length / 2 + 1];
            newChars[newChars.length - 1] = chars[chars.length - 1];
        }
        for (int i = 0; i < chars.length / 2; i++) {
            newChars[i] = (char) ('a' + (chars[2 * i] ^ chars[2 * i + 1]) % ('z' - 'a'));
        }
        return new String(newChars);
    }
}
