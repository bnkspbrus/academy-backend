package encoder;

public interface Encoder {
    public String encode(String string, int maxSize);
}
