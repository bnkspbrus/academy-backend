package util;

import java.util.Random;

public class GeneratorUtils {
    public static int[] genIndexes(int max, int size) {
        if (max == 0) {
            return new int[]{};
        }
        Random random = new Random();
        return random
                .ints(size, 0, max)
                .distinct()
                .sorted()
                .toArray();
    }
}
