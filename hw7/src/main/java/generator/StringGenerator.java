package generator;

public interface StringGenerator {
    String generate(int size, boolean withDigits, boolean withSpecChars);
}
