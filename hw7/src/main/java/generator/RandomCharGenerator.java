package generator;

import java.util.Random;

import static util.GeneratorUtils.genIndexes;

public class RandomCharGenerator {

    private final Random random = new Random();
    private final String SPEC_CHARS = " !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";

    public char generate(boolean withDigits, boolean withSpecChars) {
        switch (random.nextInt(3)) {
            case 0 -> {
                return genLetter();
            }
            case 1 -> {
                if (withDigits)
                    return genDigit();
                else
                    return genLetter();
            }
            default -> {
                if (withSpecChars)
                    return genSpecChar();
                else
                    return genLetter();
            }
        }
    }

    private char genSpecChar() {
        return SPEC_CHARS.charAt(random.nextInt(SPEC_CHARS.length()));
    }

    private char genDigit() {
        return (char) random.nextInt('0', '9' + 1);
    }

    private char genLetter() {
        char gen = (char) random.nextInt('a', 'z' + 1);
        if (random.nextBoolean()) {
            return Character.toUpperCase(gen);
        }
        return gen;
    }
}
