package generator;

import encoder.Encoder;
import encoder.StringEncoder;
import util.GeneratorUtils;

import java.util.Scanner;

public class PasswordGenerator implements StringGenerator {
    private final RandomCharGenerator randomCharGenerator = new RandomCharGenerator();
    private final Encoder stringEncoder = new StringEncoder();

    private final String passphrase;

    public PasswordGenerator(String passphrase) {
        this.passphrase = passphrase;
    }

    @Override
    public String generate(int size, boolean includeDigits, boolean includeSpecChars) {
        String password = stringEncoder.encode(passphrase, size);
        int[] indexes = GeneratorUtils.genIndexes(password.length(), size - password.length());
        StringBuilder passwordBuilder = new StringBuilder(password);
        for (int i = 0; i < indexes.length; i++) {
            passwordBuilder.insert(
                    i + indexes[i],
                    randomCharGenerator.generate(includeDigits, includeSpecChars)
            );
        }
        password = passwordBuilder.toString();
        return password;
    }

    private static void runInteractive() {
        Scanner lineReader = new Scanner(System.in);
        System.out.print("Enter secret passphrase: ");
        String passphrase = lineReader.nextLine();
        System.out.print("Enter password size (default 8): ");
        int size = 8;
        Scanner scanner = new Scanner(lineReader.nextLine());
        if (scanner.hasNextInt()) {
            size = scanner.nextInt();
        }
        System.out.print("Password contains digits [yes, no] (default yes): ");
        boolean includeDigits = true;
        scanner = new Scanner(lineReader.nextLine());
        if (scanner.hasNext()) {
            if (scanner.next().equals("no")) {
                includeDigits = false;
            }
        }
        System.out.print("Password contains special characters [yes, no] (default yes): ");
        boolean includeSpecChars = true;
        scanner = new Scanner(lineReader.nextLine());
        if (scanner.hasNextLine()) {
            if (scanner.next().equals("no")) {
                includeSpecChars = false;
            }
        }
        System.out.println(new PasswordGenerator(passphrase).generate(size, includeDigits, includeSpecChars));
    }

    public static void main(String[] args) {
        runInteractive();
    }
}
