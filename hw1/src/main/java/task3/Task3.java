package task3;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Task3 {
    public static void main(String[] args) {
        Printer printer = new Printer();
        Thread[] threads = IntStream.rangeClosed(1, 3).
                mapToObj(i -> new Thread(new Worker(printer, i)))
                .toArray(Thread[]::new);
        Arrays.stream(threads).forEach(Thread::start);
        Arrays.stream(threads).forEach(thread -> {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }


    /**
     * Printer is lock object for workers.
     */
    private static final class Printer {
        public int onPrint = 1;
    }

    /**
     * Worker prints its own digit to stdout in parallel with other.
     *
     * @param printer is a lock object. It needs to synchronize workers' output.
     * @param toPrint is a worker's digit to be printed.
     */
    private record Worker(Printer printer, int toPrint) implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 6; i++) {
                try {
                    synchronized (printer) {
                        while (toPrint != printer.onPrint) {
                            printer.wait();
                        }
                        System.out.print(toPrint);
                        printer.onPrint = 1 + toPrint % 3;
                        printer.notifyAll();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}

