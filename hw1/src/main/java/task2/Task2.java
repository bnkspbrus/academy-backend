package task2;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Task2 {
    public Map<Integer, Long> getDuplicateMap(List<Integer> list) {
        Map<Integer, Long> counting = list.stream().
                collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return counting.entrySet().stream().filter(this::isDuplicate).
                collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    private boolean isDuplicate(Map.Entry<Integer, Long> entry) {
        return entry.getValue() > 1;
    }
}
