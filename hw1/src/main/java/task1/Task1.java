package task1;

import java.util.List;

public class Task1 {
    public List<Integer> square56(List<Integer> list) {
        return list.stream().map(this::squareAndAdd10).filter(this::ends56).toList();
    }

    private boolean ends56(int x) {
        return x % 10 != 5 && x % 10 != 6;
    }

    private int squareAndAdd10(int x) {
        return x * x + 10;
    }
}
