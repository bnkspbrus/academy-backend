package task4;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Article needs to be supplied and consumed.
 */
class Article {
    private static final AtomicInteger count = new AtomicInteger(0);

    private final int id = count.incrementAndGet();

    @Override
    public String toString() {
        return "Article " + id;
    }
}

/**
 * Supplier needs to supply an article and put it in the queue.
 */
class Supplier implements Runnable {
    private static int count = 0;

    private final int id = ++count;

    private final BlockingQueue<Article> articles;

    private final Random rand = new Random();

    public Supplier(BlockingQueue<Article> articles) {
        this.articles = articles;
    }

    public Article supply() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(
                100 + rand.nextInt(500));
        return new Article();
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                Article t = supply();
                System.out.println(t + " supplied with " + this);
                articles.put(t);
            }
        } catch (InterruptedException e) {
            System.out.println(this + " interrupted");
        }
        System.out.println(this + " off");
    }

    @Override
    public String toString() {
        return "Supplier " + id;
    }
}

/**
 * Consumer needs to consume an article from the queue.
 */
class Consumer implements Runnable {

    private static int count = 0;

    private final int id = ++count;
    private final BlockingQueue<Article> articles;

    private final Random rand = new Random();

    public Consumer(BlockingQueue<Article> articles) {
        this.articles = articles;
    }

    public void consume() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(
                100 + rand.nextInt(500));
    }

    @Override
    public void run() {
        try {
            while (!Thread.interrupted()) {
                Article t = articles.take();
                consume();
                System.out.println(t + " consumed with " + this);
            }
        } catch (InterruptedException e) {
            System.out.println(this + " interrupted");
        }
        System.out.println(this + " off");
    }

    @Override
    public String toString() {
        return "Consumer " + id;
    }
}

public class Task4 {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Article> articles = new LinkedBlockingQueue<>();
        ExecutorService exec = Executors.newCachedThreadPool();
        for (int i = 0; i < 2; i++) {
            exec.execute(new Supplier(articles));
        }
        for (int i = 0; i < 5; i++) {
            exec.execute(new Consumer(articles));
        }
        TimeUnit.SECONDS.sleep(5);
        exec.shutdownNow();
    }
}
