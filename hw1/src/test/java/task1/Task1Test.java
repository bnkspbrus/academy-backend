package task1;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Task1Test {

    private Task1 task1;

    @Before
    public void initialize() {
        task1 = new Task1();
    }

    @Test
    public void square56() {
        assertEquals(Arrays.asList(19, 11), task1.square56(Arrays.asList(3, 1, 4)));
        assertEquals(List.of(11), task1.square56(List.of(1)));
        assertEquals(List.of(14), task1.square56(List.of(2)));
    }
}