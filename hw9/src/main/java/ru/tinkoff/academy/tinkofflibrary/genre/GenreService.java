package ru.tinkoff.academy.tinkofflibrary.genre;

import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.AbstractRepository;
import ru.tinkoff.academy.tinkofflibrary.AbstractService;

@Service
public class GenreService extends AbstractService<Genre, CreatingGenreDto> {

    public GenreService(AbstractRepository<Genre> repository) {
        super(repository);
    }

    @Override
    protected String getEntityName() {
        return "Genre";
    }

    public Genre save(CreatingGenreDto creatingGenreDto) {
        return repository.save(Genre
                .builder()
                .name(creatingGenreDto.getName())
                .build());
    }
}
