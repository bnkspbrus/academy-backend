package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.AbstractRepository;
import ru.tinkoff.academy.tinkofflibrary.AbstractService;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class AuthorService extends AbstractService<Author, CreatingAuthorDto> {

    public AuthorService(AuthorRepository repository) {
        super(repository);
    }

    @Override
    protected String getEntityName() {
        return "Author";
    }

    public Author save(CreatingAuthorDto creatingAuthorDto) {
        return repository.save(Author
                .builder()
                .name(creatingAuthorDto.getName())
                .build()
        );
    }
}
