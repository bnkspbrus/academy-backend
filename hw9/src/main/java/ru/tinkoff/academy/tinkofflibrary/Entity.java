package ru.tinkoff.academy.tinkofflibrary;

public interface Entity {
    Long getId();
    void setId(Long id);
}
