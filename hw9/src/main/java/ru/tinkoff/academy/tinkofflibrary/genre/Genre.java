package ru.tinkoff.academy.tinkofflibrary.genre;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.academy.tinkofflibrary.Entity;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Genre implements Entity {
    private Long id;
    private String name;
}
