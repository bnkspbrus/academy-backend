package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.tinkoff.academy.tinkofflibrary.Entity;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Book implements Entity {
    private Long id;
    private String name;
    private String authorName;
    private LocalDate publicationDate;
    private String genreName;
}
