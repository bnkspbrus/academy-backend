package ru.tinkoff.academy.tinkofflibrary;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends Entity> {
    protected final List<E> entities = new ArrayList<>();

    public E save(E entity) {
        long lastId = entities.isEmpty() ? -1 : entities.get(entities.size() - 1).getId();
        entity.setId(lastId + 1);
        entities.add(entity);
        return entity;
    }

    public void deleteById(Long id) {
        Optional<E> entity = findById(id);
        entity.ifPresent(entities::remove);
    }

    public Optional<E> findById(Long id) {
        return entities.stream().filter(entity -> entity.getId().equals(id)).findAny();
    }

    public List<E> findAll() {
        return entities;
    }
}
