package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.AbstractRepository;
import ru.tinkoff.academy.tinkofflibrary.AbstractService;
import ru.tinkoff.academy.tinkofflibrary.author.Author;
import ru.tinkoff.academy.tinkofflibrary.author.AuthorService;
import ru.tinkoff.academy.tinkofflibrary.genre.Genre;
import ru.tinkoff.academy.tinkofflibrary.genre.GenreService;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Predicate;

@Service
public class BookService extends AbstractService<Book, CreatingBookDto> {

    /**
     * ДЗ:
     * - Реализовать контроллер, репозиторий и сервис для сущностей Genre и Author
     * - Иметь возможность найти автора и жанр по id. Имплементировать логику, которая приведена ниже в комментариях
     */
    private final AuthorService authorService;
    private final GenreService genreService;

    public BookService(AbstractRepository<Book> repository, AuthorService authorService, GenreService genreService) {
        super(repository);
        this.authorService = authorService;
        this.genreService = genreService;
    }

    public Book save(CreatingBookDto bookDto) {
        Author author = authorService.getById(bookDto.getAuthorId());
        Genre genre = genreService.getById(bookDto.getGenreId());

        Book bookForSaving = Book.builder()
                .name(bookDto.getName())
                .authorName(author.getName())
                .genreName(genre.getName())
                .publicationDate(bookDto.getPublicationDate())
                .build();

        return repository.save(bookForSaving);
    }

    @Override
    protected String getEntityName() {
        return "Book";
    }

    public List<Book> findByAuthor(String authorName) {
        return ((BookRepository) repository).findByAuthor(authorName);
    }

    public List<Book> findByPredicate(Predicate<Book> predicate) {
        return ((BookRepository) repository).findByPredicate(predicate);
    }
}
