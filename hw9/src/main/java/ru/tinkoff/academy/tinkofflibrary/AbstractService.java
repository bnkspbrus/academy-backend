package ru.tinkoff.academy.tinkofflibrary;

import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public abstract class AbstractService<E extends Entity, D> {
    protected final AbstractRepository<E> repository;

    protected abstract String getEntityName();

    public E getById(Long id) {
        return repository
                .findById(id)
                .orElseThrow(() -> new RuntimeException(getEntityName() + " with id=" + id + " not found"));
    }

    public Optional<E> findById(Long id) {
        return repository.findById(id);
    }

    public abstract E save(D dto);

    public void deleteById(Long id) {
        repository.deleteById(id);
    }
}
