package ru.tinkoff.academy.tinkofflibrary.genre;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/genres")
@RequiredArgsConstructor
public class GenreController {
    private final GenreService genreService;

    @GetMapping("/{genreId}")
    public ResponseEntity<Genre> getById(@PathVariable Long genreId) {
        Optional<Genre> genre = genreService.findById(genreId);

        if (genre.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(genre.get(), HttpStatus.OK);
    }

    @PostMapping
    public Genre save(@RequestBody CreatingGenreDto creatingGenreDto) {
        return genreService.save(creatingGenreDto);
    }

    @DeleteMapping("/{genreId}")
    public void remove(@PathVariable Long genreId) {
        genreService.deleteById(genreId);
    }
}
