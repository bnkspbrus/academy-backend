package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/authors")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorService authorService;

    @GetMapping("/{authorId}")
    public ResponseEntity<Author> getById(@PathVariable Long authorId) {
        Optional<Author> author = authorService.findById(authorId);

        if (author.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(author.get(), HttpStatus.OK);
    }

    @PostMapping
    public Author save(@RequestBody CreatingAuthorDto creatingAuthorDto) {
        return authorService.save(creatingAuthorDto);
    }

    @DeleteMapping("/{authorId}")
    public void remove(@PathVariable Long authorId) {
        authorService.deleteById(authorId);
    }
}
