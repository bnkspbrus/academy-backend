package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {

    /**
     * ДЗ:
     * - Реализовать метод для удаления сущности (use HTTP method DELETE)
     * <p>
     * - *Вместо метода getAll сделать метод search, который на основе параметров запроса будет
     * фильтровать сущности по ее параметрам. Если никакие параметры не указаны, то будут возвращаться
     * все сущности из репозитория. Ожидаемый url:
     * <p>
     * localhost:8080/books/search?author=pushkin&genre=non-fiction
     * ** Используйте @RequestParam **
     */

    private final BookService bookService;

    @GetMapping("/search")
    public List<Book> search(
            @RequestParam(required = false) String id,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String authorName,
            @RequestParam(required = false) String publicationDate,
            @RequestParam(required = false) String genreName
    ) {
        Predicate<Book> predicate = book -> true;
        if (id != null) {
            predicate = predicate.and(book -> book.getId() == Long.parseLong(id));
        }
        if (name != null) {
            predicate = predicate.and(book -> book.getName().equals(name));
        }
        if (authorName != null) {
            predicate = predicate.and(book -> book.getAuthorName().equals(authorName));
        }
        if (publicationDate != null) {
            predicate = predicate.and(book -> book.getPublicationDate().equals(LocalDate.parse(publicationDate)));
        }
        if (genreName != null) {
            predicate = predicate.and(book -> book.getGenreName().equals(genreName));
        }
        return bookService.findByPredicate(predicate);
    }

    @GetMapping("/{bookId}")
    public ResponseEntity<Book> getById(@PathVariable Long bookId) {
        Optional<Book> book = bookService.findById(bookId);

        if (book.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(book.get(), HttpStatus.OK);
    }

    @PostMapping
    public Book save(@RequestBody CreatingBookDto creatingBookDto) {
        return bookService.save(creatingBookDto);
    }

    @DeleteMapping("/{bookId}")
    public void remove(@PathVariable Long bookId) {
        bookService.deleteById(bookId);
    }
}
