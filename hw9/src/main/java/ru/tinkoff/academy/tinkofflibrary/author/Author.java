package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.*;
import ru.tinkoff.academy.tinkofflibrary.Entity;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Author implements Entity {
    private Long id;
    private String name;
}
