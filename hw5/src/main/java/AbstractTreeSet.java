import java.util.AbstractSet;
import java.util.Objects;
import java.util.SortedSet;

public abstract class AbstractTreeSet<E extends Comparable<E>> extends AbstractSet<E> {
    private Node<E> root = null;

    @Override
    public int size() {
        return Node.getSize(root);
    }

    @Override
    public boolean add(E element) {
        if (contains(element)) {
            return false;
        }
        Node<E> node = new Node<>(element);
        root = insertNode(root, node);
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean remove(Object object) {
        E cast = (E) object;
        if (!contains(cast)) {
            return false;
        }
        root = removeNode(root, cast);
        return true;
    }

    public E findMin() {
        return findMin(root);
    }

    public E findMin(Node<E> root) {
        if (Objects.isNull(root)) {
            return null;
        }
        Node<E> current = root;
        while (Objects.nonNull(current.getLeft())) {
            current = current.getLeft();
        }
        return current.getElement();
    }

    public E findMax() {
        return findMax(root);
    }

    public E findMax(Node<E> root) {
        if (Objects.isNull(root)) {
            return null;
        }
        Node<E> current = root;
        while (Objects.nonNull(current.getRight())) {
            current = current.getRight();
        }
        return current.getElement();
    }

    private Node<E> insertNode(Node<E> root, Node<E> node) {
        if (Objects.isNull(root)) {
            return node;
        }
        if (root.compareTo(node) == 0) {
            return root;
        }
        if (root.compareTo(node) > 0) {
            root.setLeft(insertNode(root.getLeft(), node));
        } else {
            root.setRight(insertNode(root.getRight(), node));
        }
        return root;
    }

    private Node<E> removeNode(Node<E> root, E element) {
        if (Objects.isNull(root)) {
            return root;
        }
        if (element.compareTo(root.getElement()) == 0) {
            if (Objects.isNull(root.getLeft())) {
                return root.getRight();
            } else if (Objects.isNull(root.getRight())) {
                return root.getLeft();
            } else {
                root.setElement(findMin(root.getRight()));
                root.setRight(removeNode(root.getRight(), root.getElement()));
            }
        } else if (element.compareTo(root.getElement()) < 0) {
            root.setLeft(removeNode(root.getLeft(), element));
        } else {
            root.setRight(removeNode(root.getRight(), element));
        }
        return root;
    }
}
