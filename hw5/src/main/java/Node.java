import java.util.Objects;

public class Node<E extends Comparable<E>> implements Comparable<Node<E>> {
    private Node<E> left;
    private Node<E> right;

    private int size;

    private E element;

    public Node(E element) {
        left = null;
        right = null;
        this.element = element;
        size = 1;
    }

    public static int getSize(Node<?> node) {
        if (Objects.isNull(node)) {
            return 0;
        } else {
            return node.size;
        }
    }

    public Node<E> getLeft() {
        return left;
    }

    public void setLeft(Node<E> left) {
        size = computeSize(left, right);
        this.left = left;
    }

    public Node<E> getRight() {
        return right;
    }

    public void setRight(Node<E> right) {
        size = computeSize(left, right);
        this.right = right;
    }

    public E getElement() {
        return element;
    }

    public void setElement(E element) {
        this.element = element;
    }

    @Override
    public int compareTo(Node<E> other) {
        return element.compareTo(other.getElement());
    }

    private int computeSize(Node<E> left, Node<E> right) {
        return getSize(left) + getSize(right) + 1;
    }
}