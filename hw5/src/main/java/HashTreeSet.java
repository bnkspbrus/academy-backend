import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class HashTreeSet<E extends Comparable<E>> extends AbstractTreeSet<E> {
    private final List<E> hashTable = new ArrayList<>();

    @SuppressWarnings("unchecked")
    @Override
    public boolean contains(Object object) {
        E cast = (E) object;
        int hash = cast.hashCode();
        return hash < hashTable.size() && Objects.nonNull(hashTable.get(hash));
    }

    @Override
    public boolean add(E element) {
        if (contains(element)) {
            return false;
        }
        super.add(element);
        int hash = element.hashCode();
        ensureHashTableSize(hash + 1);
        hashTable.set(hash, element);
        return true;
    }

    @Override
    public boolean remove(Object object) {
        if (!contains(object)) {
            return false;
        }
        super.remove(object);
        int hash = object.hashCode();
        hashTable.set(hash, null);
        return true;
    }

    //Dummy realization.
    @Override
    public Iterator<E> iterator() {
        return hashTable.iterator();
    }

    private void ensureHashTableSize(int size) {
        while (hashTable.size() < size) {
            hashTable.add(null);
        }
    }
}
