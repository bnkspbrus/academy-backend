import org.junit.Assert;
import org.junit.Test;

public class HashTreeSetTest {

    @Test
    public void testContains() {
        HashTreeSet<Integer> set = new HashTreeSet<>();
        Assert.assertTrue(set.add(1));
        Assert.assertTrue(set.contains(1));
        Assert.assertFalse(set.contains(2));
        Assert.assertFalse(set.add(1));
    }

    @Test
    public void testRemove() {
        HashTreeSet<Integer> set = new HashTreeSet<>();
        Assert.assertTrue(set.add(1));
        Assert.assertTrue(set.contains(1));
        Assert.assertTrue(set.remove(1));
        Assert.assertFalse(set.contains(1));
    }

    @Test
    public void testFindMin() {
        HashTreeSet<Integer> set = new HashTreeSet<>();
        Assert.assertTrue(set.add(4));
        Assert.assertTrue(set.add(3));
        Assert.assertTrue(set.add(1));
        int minInt = set.findMin();
        Assert.assertEquals(1, minInt);
    }
}