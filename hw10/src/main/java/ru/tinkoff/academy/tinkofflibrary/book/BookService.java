package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.AbstractService;
import ru.tinkoff.academy.tinkofflibrary.core.exception.EntityNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
public class BookService extends AbstractService<Book> {

    public BookService(BookRepository repository) {
        super(repository);
    }

    public List<Book> findAll(Pageable request) {
        return repository.findAll(request).getContent();
    }

    public Book findByTitle(String bookTitle) {
        return ((BookRepository) repository).findByTitle(bookTitle)
                .orElseThrow(() -> new EntityNotFoundException("Book not found by title=" + bookTitle));
    }

    public Book findByPrice(double price) {
        return ((BookRepository) repository).findByPrice(price).orElseThrow(
                () -> new EntityNotFoundException("Book not found by price=" + price));
    }

    @Override
    protected String getEntityName() {
        return "Book";
    }

    public List<Book> findAllCheaperThen(double price) {
        return ((BookRepository) repository).findAll(SpecificationUtils.cheaperThen(price));
    }

    public List<Book> findAllPriceBetween(double lower, double upper) {
        return ((BookRepository) repository).findAll(SpecificationUtils.priceBetween(lower, upper));
    }

    public List<Book> findAllTitleContains(String substring) {
        return ((BookRepository) repository).findAll(SpecificationUtils.titleContains(substring));
    }
}
