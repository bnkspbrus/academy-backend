package ru.tinkoff.academy.tinkofflibrary.publisher;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/publisher")
@RequiredArgsConstructor
public class PublisherController {
    private final PublisherService publisherService;

    @GetMapping("/{publisherId}")
    public ResponseEntity<Publisher> findById(@PathVariable Long publisherId) {
        Optional<Publisher> book = publisherService.findById(publisherId);

        if (book.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(book.get(), HttpStatus.OK);
    }

    @GetMapping
    public List<Publisher> findAll() {
        return publisherService.findAll();
    }

    @PostMapping
    public Publisher save(@RequestBody Publisher publisher) {
        return publisherService.save(publisher);
    }

    @DeleteMapping("/{publisherId}")
    public void deleteById(@PathVariable Long publisherId) {
        publisherService.deleteById(publisherId);
    }

    @PutMapping("/{publisherId}")
    public Publisher updateById(@PathVariable Long publisherId, @RequestBody Publisher publisher) {
        return publisherService.updateById(publisherId, publisher);
    }
}
