package ru.tinkoff.academy.tinkofflibrary;

public interface IdEntity {
    Long getId();
    void setId(Long id);
}
