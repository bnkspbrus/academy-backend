package ru.tinkoff.academy.tinkofflibrary.publisher;

import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.AbstractService;

@Service
public class PublisherService extends AbstractService<Publisher> {

    public PublisherService(PublisherRepository repository) {
        super(repository);
    }

    @Override
    protected String getEntityName() {
        return "Publisher";
    }
}
