package ru.tinkoff.academy.tinkofflibrary.book;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;

    @GetMapping
    public List<Book> findAll() {
        return bookService.findAll();
    }

    @GetMapping("/{bookId}")
    public ResponseEntity<Book> findById(@PathVariable Long bookId) {
        Optional<Book> book = bookService.findById(bookId);

        if (book.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(book.get(), HttpStatus.OK);
    }

    @GetMapping("/title")
    public Book findByTitle(@RequestParam String bookTitle) {
        return bookService.findByTitle(bookTitle);
    }

    @GetMapping("/price")
    public Book findByPrice(@RequestParam double price) {
        return bookService.findByPrice(price);
    }

    @GetMapping("/page")
    public List<Book> findAll(@RequestParam String page, @RequestParam String size) {
        Pageable request = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
        return bookService.findAll(request);
    }

    @GetMapping("/page/sort")
    public List<Book> findAllSort(@RequestParam String page, @RequestParam String size, @RequestParam String by) {
        Pageable request = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by(by));
        return bookService.findAll(request);
    }

    @GetMapping("/filter/price/lessThen")
    public List<Book> findAllCheaperThen(@RequestParam double price) {
        return bookService.findAllCheaperThen(price);
    }

    @GetMapping("/filter/price/between")
    public List<Book> findAllPriceBetween(@RequestParam double lower, double upper) {
        return bookService.findAllPriceBetween(lower, upper);
    }

    @GetMapping("/filter/title/contains")
    public List<Book> findAllTitleContains(@RequestParam String substring) {
        return bookService.findAllTitleContains(substring);
    }

    @PostMapping
    public Book save(@RequestBody Book book) {
        return bookService.save(book);
    }

    @PutMapping("/{bookId}")
    public Book updateById(@PathVariable Long bookId, @RequestBody Book book) {
        return bookService.updateById(bookId, book);
    }

    @DeleteMapping("/{bookId}")
    public void deleteById(@PathVariable Long bookId) {
        bookService.deleteById(bookId);
    }
}
