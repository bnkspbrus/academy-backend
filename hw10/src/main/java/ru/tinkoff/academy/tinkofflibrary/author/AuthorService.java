package ru.tinkoff.academy.tinkofflibrary.author;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.AbstractService;
import ru.tinkoff.academy.tinkofflibrary.core.exception.EntityNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService extends AbstractService<Author> {
    public AuthorService(AuthorRepository repository) {
        super(repository);
    }

    @Override
    protected String getEntityName() {
        return "Author";
    }
}
