package ru.tinkoff.academy.tinkofflibrary;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.tinkoff.academy.tinkofflibrary.core.exception.EntityNotFoundException;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public abstract class AbstractService<E extends IdEntity> {
    protected final JpaRepository<E, Long> repository;

    protected abstract String getEntityName();

    public Optional<E> findById(Long id) {
        return repository.findById(id);
    }

    public List<E> findAll() {
        return repository.findAll();
    }

    public E save(E entity) {
        return repository.save(entity);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public E updateById(Long id, E newEntity) {
        Optional<E> entity = findById(id);
        if (entity.isEmpty()) {
            throw new EntityNotFoundException(getEntityName() + " not found by id=" + id);
        }
        newEntity.setId(id);
        repository.save(newEntity);
        return newEntity;
    }
}
