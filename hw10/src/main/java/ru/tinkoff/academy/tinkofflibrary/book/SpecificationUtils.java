package ru.tinkoff.academy.tinkofflibrary.book;

import org.springframework.data.jpa.domain.Specification;

public class SpecificationUtils {
    public static Specification<Book> cheaperThen(double price) {
        return (book, query, builder) -> builder.lt(book.get("price"), price);
    }

    public static Specification<Book> priceBetween(double lower, double upper) {
        return (book, query, builder) -> builder.between(book.get("price"), lower, upper);
    }

    public static Specification<Book> titleContains(String substring) {
        return (book, query, builder) -> builder.like(book.get("title"), "%" + substring + "%");
    }
}
