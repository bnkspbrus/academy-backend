package ru.tinkoff.academy.tinkofflibrary.address;

import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ru.tinkoff.academy.tinkofflibrary.AbstractService;
import ru.tinkoff.academy.tinkofflibrary.core.exception.EntityNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService extends AbstractService<Address> {
    public AddressService(JpaRepository<Address, Long> repository) {
        super(repository);
    }

    @Override
    protected String getEntityName() {
        return "Address";
    }
}
