package ru.tinkoff.academy.tinkofflibrary.address;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;

    @GetMapping("/{addressId}")
    public ResponseEntity<Address> findById(@PathVariable Long addressId) {
        Optional<Address> address = addressService.findById(addressId);

        if (address.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(address.get(), HttpStatus.OK);
    }

    @GetMapping
    public List<Address> findAll() {
        return addressService.findAll();
    }

    @PostMapping
    public Address save(@RequestBody Address address) {
        return addressService.save(address);
    }

    @DeleteMapping("/{addressId}")
    public void deleteById(@PathVariable Long addressId) {
        addressService.deleteById(addressId);
    }

    @PutMapping("/{addressId}")
    public Address updateById(@PathVariable Long addressId, @RequestBody Address address) {
        return addressService.updateById(addressId, address);
    }
}
