package ru.tinkoff.academy.tinkofflibrary.genre;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/genre")
@RequiredArgsConstructor
public class GenreController {
    private final GenreService genreService;

    @GetMapping("/{genreId}")
    public ResponseEntity<Genre> findById(@PathVariable Long genreId) {
        Optional<Genre> book = genreService.findById(genreId);

        if (book.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(book.get(), HttpStatus.OK);
    }

    @GetMapping
    public List<Genre> findAll() {
        return genreService.findAll();
    }

    @PostMapping
    public Genre save(@RequestBody Genre genre) {
        return genreService.save(genre);
    }

    @DeleteMapping("/{genreId}")
    public void deleteById(@PathVariable Long genreId) {
        genreService.deleteById(genreId);
    }

    @PutMapping("/{genreId}")
    public Genre updateById(@PathVariable Long genreId, @RequestBody Genre genre) {
        return genreService.updateById(genreId, genre);
    }
}
